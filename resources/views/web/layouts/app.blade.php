
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{{$title ?? ''}} | charitablecharm</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="donation" />
	<meta name="keywords" content="donation" />
	<meta name="author" content="FREEHTML5.CO" />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">



	@include('web.layouts.includes.styles')

    <!-- Modernizr JS -->
	<script src="{{ $web_source }}/assets/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->


	


	</head>
	<body>
		<div id="fh5co-wrapper">
            <div id="fh5co-page">
                @include('web.layouts.includes.navbar')
                @yield('content')
            </div>
        </div>

        <!-- END fh5co-wrapper -->

        @include('web.layouts.includes.scripts')

	</body>
</html>

