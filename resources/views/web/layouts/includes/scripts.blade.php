<!-- jQuery -->


	<script src="{{ $web_source }}/assets/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="{{ $web_source }}/assets/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="{{ $web_source }}/assets/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="{{ $web_source }}/assets/js/jquery.waypoints.min.js"></script>
	<script src="{{ $web_source }}/assets/js/sticky.js"></script>

	<!-- Stellar -->
	<script src="{{ $web_source }}/assets/js/jquery.stellar.min.js"></script>
	<!-- Superfish -->
	<script src="{{ $web_source }}/assets/js/hoverIntent.js"></script>
	<script src="{{ $web_source }}/assets/js/superfish.js"></script>

	<!-- Main JS -->
	<script src="{{ $web_source }}/assets/js/main.js"></script>

    


    <!-- Google Map -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
	<script src="{{ $web_source }}/assets/js/google_map.js"></script>


