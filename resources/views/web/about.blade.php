@extends('web.layouts.app', ['title' => 'Home'])
@section('content')
<div class="fh5co-hero">
    <div class="fh5co-overlay"></div>
    <div class="fh5co-cover text-center" data-stellar-background-ratio="0.5" style="background-image: url({{ $web_source }}/assets/images/childpalestine.jpg);">
        <div class="desc animate-box">
            <h2><strong>Donate</strong> for the <strong>Poor Children</strong></h2>
            <span><a class="btn btn-primary btn-lg" href="https://www.paypal.com/donate?hosted_button_id=9QDLU7LXH7E7A">Donate Now</a></span>
        </div>
    </div>

</div>
<!-- end:header-top -->


<div id="fh5co-feature-product" class="fh5co-section-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center heading-section">
                <h3>Help kids in palestine.</h3>
                <p>.Decades of instability and conflict have already left families on the Gaza strip and the West Bank unable to enjoy the most basic of human rights such as access to food, water and medical treatment. The recent escalation in Gaza is causing devastation on a scale not seen since 2014. So far, 119 people have been killed, including 27 children, and 600 people have been wounded. [BBC]

                    There is currently a shortage of medical supplies in Gaza as well as a dire need for food. We are working with our partner to distribute food and emergency medical care in Gaza, providing families with essential supplies to help them cope with the latest crisis.

                    By donating towards our Gaza Fund, you will be supporting vulnerable families in their hour of need. Give now to alleviate suffering in Gaza.As the unending conflict continues, there are thousands of displaced kids in need of help. Donate today and provide kids with a means to survive the unending crisis</p>
            </div>
        </div>

        <div class="row row-bottom-padded-md">
            <div class="col-md-12 text-center animate-box">
                <p><img src="{{ $web_source }}/assets/images/1-3 (1).jpg" alt="Free HTML5 Bootstrap Template" class="img-responsive" style="width:100%"></p>
            </div>
            <div class="col-md-6 text-center animate-box">
                <p><img src="{{ $web_source }}/assets/images/Gaza IDP boy and his cat - ElBaba.jpg" alt="Free HTML5 Bootstrap Template" class="img-responsive" style="width: 100%;"></p>
            </div>
            <div class="col-md-6 text-center animate-box">
                <p><img src="{{ $web_source }}/assets/images/2618786-1493261968.jpg" alt="Free HTML5 Bootstrap Template" class="img-responsive" style="width: 100%;"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="feature-text">
                    <h3>GIVING TO CHARITY MAKES YOU FEEL GOOD</h3>
                    <p>Donating to charity is a major mood-booster. The knowledge that you’re helping others is hugely empowering and, in turn, can make you feel happier and more fulfilled. Research has identified a link between making a donation to charity and increased activity in the area of the brain that registers pleasure - proving that as the old adage goes, it really is far better to give than to receive.

                        Our own research into why people give supports this. We asked 700 of our generous donors to tell us what motivates them to give regularly to charity; 42% agreed the enjoyment they receive from giving as a key influence. </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="feature-text">
                    <h3>GIVING TO CHARITY INTRODUCES YOUR CHILDREN TO THE IMPORTANCE OF GENEROSITY</h3>
                    <p>Sharing the experience of donating to charity with your children shows them from a young age that they can make positive changes in the world. Children naturally love to help others, so nurturing their innate generosity is likely to mean that they grow up with a greater appreciation of what they have, and will carry on supporting charity in years to come.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="feature-text">
                    <h3>GIVING TO CHARITY ENCOURAGES FRIENDS AND FAMILY TO DO THE SAME</h3>
                    <p>Family giving creates a bond, helping to bolster relationships through a shared goal and raising more money than could otherwise be possible through individual donations. Chances are, many of your family members are already giving to charity, so working together could help you to make even more of a positive impact.</p>
                </div>
            </div>
        </div>


    </div>
</div>


<div id="fh5co-portfolio">
    <div class="container">

        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center heading-section animate-box">
                <h3>Our Gallery</h3>
                <p>Here are some of the programs we have been able to achieve in the past month.</p>
            </div>
        </div>


        <div class="row row-bottom-padded-md">
            <div class="col-md-12">
                <ul id="fh5co-portfolio-list">

                    <li class="two-third animate-box" data-animate-effect="fadeIn" style="background-image: url({{  $web_source }}/assets/images/pict_large-1-750x430.jpg); ">
                        <a href="#" class="color-3">
                            <div class="case-studies-summary">

                            </div>
                        </a>
                    </li>

                    <li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url({{  $web_source }}/assets/images/Mealprogram_Important_s.png); ">
                        <a href="#" class="color-4">
                            <div class="case-studies-summary">

                            </div>
                        </a>
                    </li>

                    <li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url({{  $web_source }}/assets/images/charityimg1.jpg); ">
                        <a href="#" class="color-5">
                            <div class="case-studies-summary">

                            </div>
                        </a>
                    </li>
                    <li class="two-third animate-box" data-animate-effect="fadeIn" style="background-image: url({{  $web_source }}/assets/images/60135347_862772387391570_5867851892515667968_o-750x430.jpg); ">
                        <a href="#" class="color-6">
                            <div class="case-studies-summary">

                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row">

        </div>


    </div>
</div>



<!-- fh5co-content-section -->

<div id="fh5co-services-section">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
                <h3>Our Projects. Support Us</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit est facilis maiores, perspiciatis accusamus asperiores sint consequuntur debitis.</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row text-center">
            <div class="col-md-4 col-sm-4">
                <div class="services animate-box">
                    <span><i class="icon-heart"></i></span>
                    <h3>Food Provision Project in Syria</h3>
                    <p>Children and youth growing up in poverty typically lack proper health care and nutrition. They’re also more likely to be exposed to the stress of domestic violence, gangs and drugs. All of this can impair brain development, negatively impacting long-term physical and mental well-being.
                        That's why we are determined at Charitable Charm to give the less privileged in Syria the health care and nutritions they deserve. The motion is "Empower the less privileged for the life the deserved"!.
                        This course can only be achieved with your support and donation......</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="services animate-box">
                    <span><i class="icon-heart"></i></span>
                    <h3>Shelter Provision for Palestine</h3>
                    <p>In recent conflicts between Israel and Palestine, fatalities have been reported among Palestinian children, innocent victims often killed by mistake but sometimes callously shot for no reason.
                        In the Gaza Strip where Israel declared war against Hamas, Israel confrontations and raids have resulted in the deaths of children. The attacks often target public places that have turned into refuges for civilians, like schools, hospitals, etc. In addition to the lost lives, tens of thousands of children are injured and some are left disabled for life.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="services animate-box">
                    <span><i class="icon-heart"></i></span>
                    <h3>Kenya Project</h3>
                    <p>Kenya suffers from a severe water crisis due to multiple causes, including droughts, forest degradation, floods, a lack of water supply management, the contamination of water, and population growth. ... Similarly, Kenya's population is projected to grow for the next few decades. This has lead to poor growth in children and serious health challenges amongs the children and inhabitants. Let's show support to the people of kenya by providing them with the basic amenities like water and food through our donation.</p>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- END What we do -->



<!-- fh5co-blog-section -->
@include('web.layouts.includes.footer')
@endsection