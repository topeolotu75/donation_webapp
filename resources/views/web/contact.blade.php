@extends('web.layouts.app', ['title' => 'Contact Us'])
@section('content')
<div class="fh5co-hero">
    <div class="fh5co-overlay"></div>
    <div class="fh5co-cover text-center" data-stellar-background-ratio="0.5" style="background-image: url({{ $web_source }}/assets/images/DSC06318-e1619562785657.jpg);">
        <div class="desc animate-box">
            <h2><strong>Contact</strong> Us</h2>
            <span><a class="btn btn-primary btn-lg" href="#">Donate Now</a></span>
        </div>
    </div>

</div>

<div id="fh5co-contact" class="animate-box">
    <div class="container">
        <form action="#">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="section-title">Our Address</h3>
                    <ul class="contact-info">
                        <li><i class="icon-location-pin"></i>3323 Chapel Creek Drive
                            Dallas Texas 75220</li>
                        <li><i class="icon-phone2"></i>+19728420476</li>
                        <li><i class="icon-mail"></i><a href="mailto:contact@charitablecharm.org">contact@charitablecharm.org</a></li>
                        <li><i class="icon-globe2"></i><a href="https://cahritablecharm.org">www.cahritablecharm.org</a></li>
                    </ul>
                </div>

            </div>
        </form>
    </div>
</div>
<!-- END fh5co-contact -->

<!-- END map -->
@include('web.layouts.includes.footer')
@endsection
